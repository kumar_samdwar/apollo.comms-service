package com.goldfarm.comm.factories.system;

import com.goldfarm.comm.constant.Constant;
import org.hibernate.Session;
import org.hibernate.query.Query;
import com.goldfarm.comm.dao.models.system.SystemConfigDTO;

import java.math.BigInteger;

public class ConfigHelper {

    public static final BigInteger CONFIG_SMS_TYPE = BigInteger.valueOf(10L);

    public static SystemConfigDTO getSystemConfig(Session session) {
        Query query = session.createQuery(" From SystemConfigDTO BC");
        query.setMaxResults(1);
        return (SystemConfigDTO) query.getSingleResult();
    }

    public static SystemConfigDTO getSmsConfig(Session session) {
        Query query = session.createQuery(" From SystemConfigDTO SC where SC.configType = :configType and SC.statusId = :statusId");
        query.setParameter("configType", CONFIG_SMS_TYPE);
        query.setParameter("statusId", BigInteger.valueOf(Constant.ACTIVE));
        query.setMaxResults(1);

        Object obj = query.getSingleResult();
        SystemConfigDTO systemConfigDTO = null;
        if(obj != null) {
            systemConfigDTO = (SystemConfigDTO) obj;
        }
        return systemConfigDTO;
    }
}
