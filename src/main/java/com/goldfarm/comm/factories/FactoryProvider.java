package com.goldfarm.comm.factories;

import org.hibernate.SessionFactory;

public class FactoryProvider {
    SessionFactory sessionFactory = null;

    private static FactoryProvider factoryProvider = null;

    public static FactoryProvider getInstance() {
        if(factoryProvider == null) {
            factoryProvider = new FactoryProvider();
        }

        return factoryProvider;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
