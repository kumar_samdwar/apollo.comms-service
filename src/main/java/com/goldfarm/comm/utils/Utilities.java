package com.goldfarm.comm.utils;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by chetan on 5/7/17.
 */
public class Utilities {
    private static char[] VALID_CHARACTERS =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456879".toCharArray();

    public static synchronized String generateMessageId(int numOfCharacters) {

        SecureRandom secureRandom = new SecureRandom();
        Random random = new Random();
        char[] buffer = new char[numOfCharacters];

        for (int i = 0; i < numOfCharacters; ++i) {
            // reseed rand once you've used up all available entropy bits
            if ((i % 10) == 0) {
                random.setSeed(secureRandom.nextLong()); // 64 bits of random!
            }
            buffer[i] = VALID_CHARACTERS[random.nextInt(VALID_CHARACTERS.length)];
        }
        return new StringBuffer().append(buffer).toString();
    }

    public static final boolean isNullorEmpty(String value) {
        return (value == null || value.trim().length() == 0);
    }
}
