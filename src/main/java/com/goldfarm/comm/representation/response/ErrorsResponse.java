package com.goldfarm.comm.representation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by umashankar on 28/10/17.
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorsResponse extends BaseResponse {
    @JsonProperty
    public String message;

    public ErrorsResponse(Integer status, Integer statusCode, Integer httpStatusCode, String message) {
        super(status, statusCode, httpStatusCode);
        this.message = message;
    }
}
