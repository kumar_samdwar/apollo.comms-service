package com.goldfarm.comm.representation.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.jackson.JsonSnakeCase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by chetan on 6/7/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonSnakeCase
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse {
    @JsonProperty
    public Integer status; // can be only success or failure

    @JsonProperty("status_code")
    public Integer statusCode;

    @JsonProperty("http_code")
    public Integer httpStatusCode;
}
