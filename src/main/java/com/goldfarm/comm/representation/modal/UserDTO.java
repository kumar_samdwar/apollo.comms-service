package com.goldfarm.comm.representation.modal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by umashankar on 28/10/17.
 */


@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class UserDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private Integer age;
    private Integer sex;
    private String shortName;
    private String emailId;
    private String emailIdAlternate;
    private String phone;
    private String phoneAlternate1;
    private String phoneAlternate2;
    private String phoneAlternate3;
}

