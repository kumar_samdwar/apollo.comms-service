package com.goldfarm.comm.representation.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.goldfarm.comm.constant.MessageType;
import com.goldfarm.comm.constant.PushNotificationType;
import com.goldfarm.comm.constant.TemplateType;
import io.dropwizard.jackson.JsonSnakeCase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonSnakeCase
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationRequest {
    private List<MessageType> messageTypeList;
    private PushNotificationType pushNotificationType;
    private String pushNotificationMessage;
    private String pushNotificationTitle;
    private String pushNotificationImageUrl;
    private String textMessage;
    private String emailMessageBody;
    private String emailSubject;
    private TemplateType smsTemplateType;
    private TemplateType emailTemplateType;
    private String phoneNumber;
    private String gfCode;
    private Long roleId;
}
