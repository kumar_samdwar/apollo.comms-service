package com.goldfarm.comm.representation.domainObject.modal.pushNotification;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.dropwizard.jackson.JsonSnakeCase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)

@JsonSnakeCase
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BigPushNotificationPayload extends PushNotificationPayload {
    private Data data;

    @Setter
    @Getter
    @JsonSnakeCase
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Data {

        private String message;
        private String image;

    }
}

