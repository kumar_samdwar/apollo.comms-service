package com.goldfarm.comm.representation.domainObject.modal;

import com.goldfarm.comm.representation.modal.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecipientData {
    private UserDTO userDTO;
    private String gfCode;
    private Long roleId;
}
