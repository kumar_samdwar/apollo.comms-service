package com.goldfarm.comm.validator;

import com.goldfarm.comm.ApolloCommServiceConfiguration;
import org.eclipse.jetty.http.HttpStatus;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by umashankar on 3/7/17.
 */
public class ValidateRequestServletFilter implements Filter {
    private ApolloCommServiceConfiguration apolloConfiguration;

    public ValidateRequestServletFilter(ApolloCommServiceConfiguration config) {
        apolloConfiguration = config;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        RequestValidator requestValidator = new CommServiceRequestValidator(apolloConfiguration);
        boolean validRequest = requestValidator.validateRequest(servletRequest);
        if (validRequest) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;
            httpResponse.setStatus(HttpStatus.FORBIDDEN_403);
            httpResponse.getWriter().print("ACCESS FORBIDDEN");
        }
    }

    @Override
    public void destroy() {

    }
}
