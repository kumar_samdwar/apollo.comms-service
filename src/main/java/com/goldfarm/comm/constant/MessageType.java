package com.goldfarm.comm.constant;

public enum MessageType {
    SMS("sms"),
    EMAIL("email"),
    PUSH_NOTIFICATION("push_notification"),
    PUSH_NOTIFICATION_BROADCAST("push_notification_broadcast");

    final String messageType;

    public String getMessageType() {
        return messageType;
    }

    MessageType(String messageType) {
        this.messageType = messageType;
    }
}
