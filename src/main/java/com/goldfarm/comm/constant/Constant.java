package com.goldfarm.comm.constant;

/**
 * Created by umashankar on 30/6/17.
 */
public class Constant {
    public static final int TOKEN_LENGTH = 100;
    public static final int VALID_TOKEN_STATUS = 1;
    public static final int INVALID_TOKEN_STATUS = 0;
    public static final String X_SESSION_ID = "X-Session-ID";
    public static final String APOLLO_WEB_SERVER = "apollo-web-server";
    public static final Long ACTIVE = 400l;
    public static final String SUCCESS = "success";
    public static final Integer SUCCESS_CODE = 1;
}
