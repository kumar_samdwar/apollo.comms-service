package com.goldfarm.comm.resources;

import com.goldfarm.comm.command.BroadCastProducerCommand;
import com.goldfarm.comm.command.Command;
import com.goldfarm.comm.command.ProducerCommand;
import com.goldfarm.comm.executor.Executor;
import com.goldfarm.comm.executor.ProducerExecutor;
import com.goldfarm.comm.representation.request.NotificationRequest;
import com.goldfarm.comm.representation.response.CommonResponse;
import com.goldfarm.comm.representation.response.ErrorsResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by umashankar on 28/10/17.
 */
@Slf4j
@Path("/message")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Api("/Communication")
public class MessagingResource {

    public MessagingResource() {
    }

    @ApiOperation(
            value = "Send new SMS ans PN",
            notes = "Return message id",
            response = CommonResponse.class)
    @ApiResponses(value = {@ApiResponse(code = 401, message = "UnAuthorised Request / Please get a valid token by Login", response = ErrorsResponse.class),
            @ApiResponse(code = 422, message = "Not able to create user", response = ErrorsResponse.class),
            @ApiResponse(code = 400, message = "Bad request, please check your JSON payload", response = ErrorsResponse.class),
            @ApiResponse(code = 201, message = "User Created", response = CommonResponse.class)})
    @POST
    @PermitAll
    public Response sendMessage(NotificationRequest notificationRequest) {
        Command sendCommand = new ProducerCommand(notificationRequest);
        Executor executor = new ProducerExecutor();
        return executor.execute(sendCommand);
    }

    @ApiOperation(
            value = "Broadcast new SMS ans PN",
            notes = "Return message id",
            response = CommonResponse.class)
    @ApiResponses(value = {@ApiResponse(code = 401, message = "UnAuthorised Request / Please get a valid token by Login", response = ErrorsResponse.class),
            @ApiResponse(code = 422, message = "Not able to create user", response = ErrorsResponse.class),
            @ApiResponse(code = 400, message = "Bad request, please check your JSON payload", response = ErrorsResponse.class),
            @ApiResponse(code = 201, message = "User Created", response = CommonResponse.class)})
    @Path("broadcast")
    @POST
    @PermitAll
    public Response broadcastMessage(NotificationRequest notificationRequest) {
        Command sendCommand = new BroadCastProducerCommand(notificationRequest);
        Executor executor = new ProducerExecutor();
        return executor.execute(sendCommand);
    }
}
