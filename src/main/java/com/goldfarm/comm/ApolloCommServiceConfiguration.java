package com.goldfarm.comm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.goldfarm.comm.representation.config.FcmConfig;
import com.goldfarm.comm.representation.config.MongoDbConfig;
import com.goldfarm.comm.representation.config.SmsServerConfig;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApolloCommServiceConfiguration extends Configuration {
    private String sessionAuthServiceUrl;
    private String sessionRoleServiceUrl;
    @JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;

    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    @JsonProperty("mongodb")
    public MongoDbConfig mongoDbConfig;

    @JsonProperty("fcm")
    public FcmConfig fcmConfig;

    @JsonProperty("sms")
    public SmsServerConfig smsServerConfig;

    @JsonProperty("httpDebugging")
    public boolean isHttpDebuggingEnabled;

}
