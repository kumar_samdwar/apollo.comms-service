package com.goldfarm.comm.messaging;

import com.goldfarm.comm.representation.domainObject.modal.RecipientData;
import com.goldfarm.comm.representation.response.NotificationResponse;

public interface ApolloMessaging {
    NotificationResponse sendMessage(RecipientData recipientData, MessageDataObject messageDataObject);

    void broadcastPushNotification(MessageDataObject messageDataObject);
}
