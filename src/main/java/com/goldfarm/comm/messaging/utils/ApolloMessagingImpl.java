package com.goldfarm.comm.messaging.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.goldfarm.comm.constant.MessageType;
import com.goldfarm.comm.constant.PushNotificationType;
import com.goldfarm.comm.constant.ResponseCodeValueMapper;
import com.goldfarm.comm.dao.models.system.SystemConfigDTO;
import com.goldfarm.comm.factories.FactoryProvider;
import com.goldfarm.comm.factories.system.ConfigHelper;
import com.goldfarm.comm.messaging.ApolloMessaging;
import com.goldfarm.comm.messaging.MessageDataObject;
import com.goldfarm.comm.representation.config.FcmConfig;
import com.goldfarm.comm.representation.domainObject.modal.RecipientData;
import com.goldfarm.comm.representation.domainObject.modal.pushNotification.BigPushNotificationPayload;
import com.goldfarm.comm.representation.domainObject.modal.pushNotification.PushNotificationPayload;
import com.goldfarm.comm.representation.domainObject.modal.pushNotification.SmallPushNotificationPayload;
import com.goldfarm.comm.representation.modal.UserDTO;
import com.goldfarm.comm.representation.response.FcmResponse;
import com.goldfarm.comm.representation.response.NotificationResponse;
import com.goldfarm.comm.utils.ApolloConfigUtils;
import com.goldfarm.comm.utils.MongoClientUtils;
import com.goldfarm.comm.utils.OkHttpClientUtils;
import com.goldfarm.comm.utils.Utilities;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.bson.Document;
import org.hibernate.Session;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

@Slf4j
public class ApolloMessagingImpl implements ApolloMessaging {
    private static final int NUMBER_OF_CHARACTERS = 25;
    private static final int FCM_BATCH_SIZE = 500;

    public ApolloMessagingImpl() {
    }

    public NotificationResponse sendMessage(RecipientData recipientData, MessageDataObject messageDataObject) {
        NotificationResponse notificationResponse = new NotificationResponse();
        String messageId = Utilities.generateMessageId(NUMBER_OF_CHARACTERS);
        notificationResponse.setMessageId(messageId);
        notificationResponse.setStatus(ResponseCodeValueMapper.SUCCESS);
        notificationResponse.setStatusCode(ResponseCodeValueMapper.SUCCESS);
        try {
            if (recipientData == null) {
                throw new Exception("Recipient data is not provided");
            } else {
                UserDTO userDTO = recipientData.getUserDTO();
                if (userDTO == null) {
                    throw new Exception("Recipient data is not complete");
                }
            }

            if (messageDataObject == null) {
                throw new Exception("Message data is not provided");
            }

            List<MessageType> messageTypes = messageDataObject.getMessageTypeList();
            if (messageTypes == null || messageTypes.size() == 0) {
                throw new Exception("Message type not defined");
            }

            for (MessageType messageType : messageTypes) {
                if (messageType == MessageType.EMAIL) {

                } else if (messageType == MessageType.SMS) {
                    String smsText = messageDataObject.getTextMessage();
                    String gfCode = recipientData.getGfCode();
                    String phoneNumber = recipientData.getUserDTO().getPhone();
                    //TODO SMS parsing based on Template, template type has to be given

                    try {
                        String responseString = sendSms(phoneNumber, URLEncoder.encode(smsText, "UTF-8"));
                        log.debug("Response SMS string:" + responseString);

                        int newLineIndex = responseString.indexOf("\n");
                        if (newLineIndex >= 0) {
                            responseString = responseString.substring(0, newLineIndex);
                        }
                        String statusString = null;

                        if (responseString != null) {
//                            if (responseString.startsWith("4")) {
//                                statusString = "failed";
//                            } else {
//                                statusString = "sent";
//                            }
                            statusString = "sent";
                        } else {
                            throw new Exception("Something went wrong, check error log");
                        }
                        Document document = new Document();
                        document.append("message", smsText)
                                .append("gf_code", gfCode)
                                .append("role_id", recipientData.getRoleId())
                                .append("recipient", phoneNumber)
                                .append("message_type", MessageType.SMS.getMessageType())
                                .append("status", statusString)
                                .append("response", responseString)
                                .append("message_id", messageId);
                        if (messageDataObject.getSmsTemplateType() != null) {
                            document.append("template_type", messageDataObject.getSmsTemplateType().getTemplateType());
                        }
                        insertCommLogs(document);

                    } catch (Exception ex) {
                        log.error(ex.getMessage(), ex);
                    }


                } else if (messageType == MessageType.PUSH_NOTIFICATION) {
                    PushNotificationType pushNotificationType = messageDataObject.getPushNotificationType();
                    MongoClient mongoClient = MongoClientUtils.getInstance().getMongoClient();
                    ApolloConfigUtils apolloConfigUtils = ApolloConfigUtils.getInstance();

                    MongoDatabase mongoDatabase = mongoClient.getDatabase(apolloConfigUtils.getConfig().getMongoDbConfig().getMongoDb());
                    MongoCollection<Document> userProfilesDevice = mongoDatabase.getCollection("user_device_profiles");

                    List<Document> documents = userProfilesDevice.find(
                            and(eq("gf_code", recipientData.getGfCode()), eq("profile_id", String.valueOf(recipientData.getRoleId())))
                    ).into(new ArrayList<>());
                    if (pushNotificationType == PushNotificationType.BIG_NOTIFICAION) {

                        for (Document document : documents) {
                            System.out.println("document = " + document);
                            BigPushNotificationPayload bigPushNotificationPayload = new BigPushNotificationPayload();
                            BigPushNotificationPayload.Data data = new BigPushNotificationPayload.Data();
                            data.setImage(messageDataObject.getPushNotificationImageUrl());
                            data.setMessage(messageDataObject.getPushNotificationMessage());
                            bigPushNotificationPayload.setData(data);
                            String fcmDeviceId = document.getString("fcm_device_id");
                            if (fcmDeviceId != null && fcmDeviceId.length() > 0) {
                                bigPushNotificationPayload.setTo(fcmDeviceId);
                            } else {
                                bigPushNotificationPayload.setTo("na");
                            }
                            String responseString = sendBigPushNotification(bigPushNotificationPayload);

                            ObjectMapper objectMapper = new ObjectMapper();
                            FcmResponse fcmResponse = objectMapper.readValue(responseString, FcmResponse.class);

                            String notificationStatus = validateNotificationDelivery(fcmResponse);

                            Document commLogDocument = new Document();
                            commLogDocument.append("message", messageDataObject.getPushNotificationMessage())
                                    .append("gf_code", recipientData.getGfCode())
                                    .append("role_id", recipientData.getRoleId())
                                    .append("recipient", document.getString("fcm_device_id"))
                                    .append("message_type", MessageType.PUSH_NOTIFICATION.getMessageType())
                                    .append("status", notificationStatus)
                                    .append("response", responseString)
                                    .append("request_payload", objectMapper.writeValueAsString(bigPushNotificationPayload))
                                    .append("message_id", messageId);

                            insertCommLogs(commLogDocument);

                            if (notificationStatus.equalsIgnoreCase("failed")) {
                                notificationResponse.setStatus(ResponseCodeValueMapper.FAILURE);
                                notificationResponse.setStatusCode(ResponseCodeValueMapper.FAILURE);
                            }
                        }
                    }
                    if (pushNotificationType == PushNotificationType.SMALL_NOTIFICAION) {
                        for (Document document : documents) {
                            System.out.println("document = " + document);
                            SmallPushNotificationPayload smallPushNotificationPayload = new SmallPushNotificationPayload();
                            SmallPushNotificationPayload.Notification notification = new SmallPushNotificationPayload.Notification();
                            notification.setTitle(messageDataObject.getPushNotificationTitle());
                            notification.setBody(messageDataObject.getPushNotificationMessage());
                            smallPushNotificationPayload.setNotification(notification);
                            String fcmDeviceId = document.getString("fcm_device_id");
                            if (fcmDeviceId != null && fcmDeviceId.length() > 0) {
                                smallPushNotificationPayload.setTo(fcmDeviceId);
                            } else {
                                smallPushNotificationPayload.setTo("na");
                            }
                            String responseString = sendSmallPushNotification(smallPushNotificationPayload);

                            ObjectMapper objectMapper = new ObjectMapper();
                            FcmResponse fcmResponse = objectMapper.readValue(responseString, FcmResponse.class);

                            String notificationStatus = validateNotificationDelivery(fcmResponse);

                            if (notificationStatus.equalsIgnoreCase("failed")) {
                                notificationResponse.setStatus(ResponseCodeValueMapper.FAILURE);
                                notificationResponse.setStatusCode(ResponseCodeValueMapper.FAILURE);
                            }

                            Document commLogDocument = new Document();
                            commLogDocument.append("message", messageDataObject.getPushNotificationMessage())
                                    .append("gf_code", recipientData.getGfCode())
                                    .append("role_id", recipientData.getRoleId())
                                    .append("recipient", document.getString("fcm_device_id"))
                                    .append("message_type", MessageType.PUSH_NOTIFICATION.getMessageType())
                                    .append("status", notificationStatus)
                                    .append("response", responseString)
                                    .append("request_payload", objectMapper.writeValueAsString(smallPushNotificationPayload))
                                    .append("message_id", messageId);
                            insertCommLogs(commLogDocument);
                        }
                    }

                }
            }
        } catch (Exception exception) {
            notificationResponse.setStatus(ResponseCodeValueMapper.FAILURE);
            notificationResponse.setStatusCode(ResponseCodeValueMapper.FAILURE);
            log.error(exception.getMessage());
            exception.printStackTrace();
        }
        return notificationResponse;
    }

    @Override
    public void broadcastPushNotification(MessageDataObject messageDataObject) {
        List<MessageType> messageTypes = messageDataObject.getMessageTypeList();
        try {
            if (messageTypes == null || messageTypes.size() == 0) {
                throw new Exception("Message type not defined");
            }
            for (MessageType messageType : messageTypes) {
                if (messageType.equals(MessageType.PUSH_NOTIFICATION) || messageType.equals(MessageType.PUSH_NOTIFICATION_BROADCAST)) {
                    PushNotificationType pushNotificationType = messageDataObject.getPushNotificationType();
                    MongoClient mongoClient = MongoClientUtils.getInstance().getMongoClient();
                    ApolloConfigUtils apolloConfigUtils = ApolloConfigUtils.getInstance();

                    MongoDatabase mongoDatabase = mongoClient.getDatabase(apolloConfigUtils.getConfig().getMongoDbConfig().getMongoDb());
                    MongoCollection<Document> userProfilesDevice = mongoDatabase.getCollection("user_device_profiles");

                    List<Document> documents = userProfilesDevice.find().into(new ArrayList<>());
                    /*List<Document> documents = userProfilesDevice.find(
                            and(eq("gf_code", "100031"))).into(new ArrayList<>());*/
                    if (pushNotificationType == PushNotificationType.BIG_NOTIFICAION) {
                        int index = 0;
                        int batchStartIndex = 0;
                        int batchEndIndex = 0;
                        List<String> registrationIds = new ArrayList<>();
                        for (Document document : documents) {
                            String fcmDeviceId = document.getString("fcm_device_id");
                            registrationIds.add(fcmDeviceId);
                            index++;
                            if (index % FCM_BATCH_SIZE == 0) {
                                batchEndIndex = batchStartIndex + FCM_BATCH_SIZE;
                                List<String> fcmDeviceIds = registrationIds.subList(batchStartIndex, batchEndIndex - 1);
                                batchStartIndex = batchStartIndex + FCM_BATCH_SIZE;
                                prepareAndSendBigNotification(messageDataObject, fcmDeviceIds, messageType);
                            }
                        }
                        prepareAndSendBigNotification(messageDataObject, registrationIds, messageType);
                    }
                    if (pushNotificationType == PushNotificationType.SMALL_NOTIFICAION) {
                        List<String> registrationIds = new ArrayList<>();
                        int index = 0;
                        int batchStartIndex = 0;
                        int batchEndIndex = 0;
                        for (Document document : documents) {
                            String fcmDeviceId = document.getString("fcm_device_id");
                            registrationIds.add(fcmDeviceId);
                            index++;
                            if (index % FCM_BATCH_SIZE == 0) {
                                batchEndIndex = batchStartIndex + FCM_BATCH_SIZE;
                                List<String> fcmDeviceIds = registrationIds.subList(batchStartIndex, batchEndIndex);
                                batchStartIndex = batchStartIndex + FCM_BATCH_SIZE;
                                prepareAndSendSmallNotification(messageDataObject, fcmDeviceIds, messageType);
                            }
                        }
                        int remainingElements = registrationIds.size() % FCM_BATCH_SIZE;
                        batchStartIndex = registrationIds.size() - remainingElements;
                        batchEndIndex = registrationIds.size();
                        List<String> fcmDeviceIds = registrationIds.subList(batchStartIndex, batchEndIndex);
                        prepareAndSendSmallNotification(messageDataObject, fcmDeviceIds, messageType);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareAndSendSmallNotification(MessageDataObject messageDataObject, List<String> registrationIds, MessageType messageType) {
        try {
            String messageId = Utilities.generateMessageId(NUMBER_OF_CHARACTERS);
            SmallPushNotificationPayload smallPushNotificationPayload = new SmallPushNotificationPayload();
            SmallPushNotificationPayload.Notification notification = new SmallPushNotificationPayload.Notification();
            notification.setTitle(messageDataObject.getPushNotificationTitle());
            notification.setBody(messageDataObject.getPushNotificationMessage());
            smallPushNotificationPayload.setNotification(notification);
            smallPushNotificationPayload.setRegistrationIds(registrationIds);
            String responseString = sendSmallPushNotification(smallPushNotificationPayload);
            ObjectMapper objectMapper = new ObjectMapper();
            FcmResponse fcmResponse = objectMapper.readValue(responseString, FcmResponse.class);
            String notificationStatus = validateNotificationDelivery(fcmResponse);
            Document commLogDocument = new Document();
            commLogDocument.append("message", messageDataObject.getPushNotificationMessage())
                    .append("recipient", registrationIds)
                    .append("message_type", messageType.getMessageType())
                    .append("status", notificationStatus)
                    .append("response", responseString)
                    .append("request_payload", objectMapper.writeValueAsString(smallPushNotificationPayload))
                    .append("message_id", messageId);
            insertCommLogs(commLogDocument);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private void prepareAndSendBigNotification(MessageDataObject messageDataObject, List<String> registrationIds, MessageType messageType) {
        String messageId = Utilities.generateMessageId(NUMBER_OF_CHARACTERS);
        try {
            BigPushNotificationPayload bigPushNotificationPayload = new BigPushNotificationPayload();
            BigPushNotificationPayload.Data data = new BigPushNotificationPayload.Data();
            data.setImage(messageDataObject.getPushNotificationImageUrl());
            data.setMessage(messageDataObject.getPushNotificationMessage());

            bigPushNotificationPayload.setData(data);
            bigPushNotificationPayload.setRegistrationIds(registrationIds);
            String responseString = sendBigPushNotification(bigPushNotificationPayload);
            ObjectMapper objectMapper = new ObjectMapper();
            FcmResponse fcmResponse = objectMapper.readValue(responseString, FcmResponse.class);

            String notificationStatus = validateNotificationDelivery(fcmResponse);

            Document commLogDocument = new Document();
            commLogDocument.append("message", messageDataObject.getPushNotificationMessage())
                    .append("recipient", registrationIds)
                    .append("message_type", messageType.getMessageType())
                    .append("status", notificationStatus)
                    .append("response", responseString)
                    .append("request_payload", objectMapper.writeValueAsString(bigPushNotificationPayload))
                    .append("message_id", messageId);

            insertCommLogs(commLogDocument);
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    private String sendSms(String phoneNumber, String smsBody) {
        Session session = FactoryProvider.getInstance().getSessionFactory().openSession();
        SystemConfigDTO systemConfigDTO = ConfigHelper.getSmsConfig(session);

        if (systemConfigDTO == null || Utilities.isNullorEmpty(systemConfigDTO.getConfigValue())) {
            return null;
        }

        String responseString = null;
        String smsApiUrl = systemConfigDTO.getConfigValue();
//                ApolloConfigUtils.getInstance().getConfig().getSmsServerConfig().getApiUrl();

        OkHttpClient okHttpClient = OkHttpClientUtils.getOkHttpClientInstance().getClient();
//        StringBuilder urlBuilder = new StringBuilder().append(smsApiUrl);
//        urlBuilder.append("&route=trans1").append("&senderid=FARMER");
//        urlBuilder.append("&numbers=").append(phoneNumber);
//        urlBuilder.append("&message=").append(smsBody);

        smsApiUrl = smsApiUrl.replace("$$$number$$$", phoneNumber);
        smsApiUrl = smsApiUrl.replace("$$$body$$$", smsBody);

        log.debug("Hitting SMS API, " + smsApiUrl);
        Request request = new Request.Builder()
                .url(smsApiUrl)
                .get()
                .build();
        ResponseBody responseBody = null;

        try {
            Response response = okHttpClient.newCall(request).execute();
            responseBody = response.body();
            responseString = responseBody.string();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {
            if (responseBody != null) {
                responseBody.close();
            }
            session.close();
        }
        return responseString;
    }

    private String sendSmallPushNotification(SmallPushNotificationPayload payload) {
        return sendPushNotification(payload);
    }

    private String sendPushNotification(PushNotificationPayload payload) {
        ApolloConfigUtils apolloConfigUtils = ApolloConfigUtils.getInstance();
        FcmConfig fcmConfig = apolloConfigUtils.getConfig().getFcmConfig();
        String fcmApiKey = fcmConfig.getApiKey();
        String fcmApiUrl = fcmConfig.getApiUrl();
        ObjectMapper objectMapper = new ObjectMapper();

        OkHttpClient client = OkHttpClientUtils.getOkHttpClientInstance().getClient();
        ResponseBody responseBody = null;
        String notificationResponse = null;
        try {
            String jsonPayload = objectMapper.writeValueAsString(payload);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonPayload);
            Request request = new Request.Builder()
                    .url(fcmApiUrl)
                    .post(body)
                    .header("Authorization", fcmApiKey)
                    .build();
            Response response = client.newCall(request).execute();
            responseBody = response.body();
            notificationResponse = responseBody.string();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (responseBody != null) {
                responseBody.close();
            }
        }
        return notificationResponse;
    }

    private String sendBigPushNotification(BigPushNotificationPayload payload) {
        return sendPushNotification(payload);
    }

    private void insertCommLogs(Document document) {
        MongoClient mongoClient = MongoClientUtils.getInstance().getMongoClient();
        ApolloConfigUtils apolloConfigUtils = ApolloConfigUtils.getInstance();

        MongoDatabase mongoDatabase = mongoClient.getDatabase(apolloConfigUtils.getConfig().getMongoDbConfig().getMongoDb());
        MongoCollection<Document> commLogsCollection = mongoDatabase.getCollection("comm_logs");

        Date now = new Date();
        document.append("timestamp", now);
        commLogsCollection.insertOne(document);
    }

    private String validateNotificationDelivery(FcmResponse fcmResponse) {
        if (fcmResponse == null) {
            return "failed";
        } else {
            if (fcmResponse.getSuccess() == 0 || fcmResponse.getFailure() == 1) {
                return "failed";
            } else {
                return "sent";
            }
        }
    }
}
