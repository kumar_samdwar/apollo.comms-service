package com.goldfarm.comm.command;

import com.goldfarm.comm.messaging.ApolloMessaging;
import com.goldfarm.comm.messaging.MessageDataObject;
import com.goldfarm.comm.messaging.utils.ApolloMessagingImpl;
import com.goldfarm.comm.representation.domainObject.modal.RecipientData;
import com.goldfarm.comm.representation.modal.UserDTO;
import com.goldfarm.comm.representation.request.NotificationRequest;
import com.goldfarm.comm.representation.response.NotificationResponse;

import javax.ws.rs.core.Response;

/**
 * Created by sam on 24/8/16.
 */
public class BroadCastProducerCommand implements Command {
    private NotificationRequest notificationRequest;

    public BroadCastProducerCommand(NotificationRequest notificationRequest) {
        this.notificationRequest = notificationRequest;
    }

    @Override
    public Response run() {
        MessageDataObject messageDataObject = MessageDataObject.messageBuilder()
                .messageTypeList(notificationRequest.getMessageTypeList())
                .pushNotificationImageUrl(notificationRequest.getPushNotificationImageUrl())
                .pushNotificationType(notificationRequest.getPushNotificationType())
                .pushNotificationMessage(notificationRequest.getPushNotificationMessage())
                .pushNotificationTitle(notificationRequest.getPushNotificationTitle())
                .build();
        ApolloMessaging apolloMessaging = new ApolloMessagingImpl();
        apolloMessaging.broadcastPushNotification(messageDataObject);
        return Response.status(Response.Status.OK).build();
    }
}
